#!/usr/bin/env pythoh3

'''
Programa para mostrar la lista de la compra
'''


# Función que elimina los elementos duplicados
def borrar_duplicados(lista):
    return list(set(lista))


# Función que pide al usuario completar la compra específica
# Escribe en la pantalla la lista completa.
def main():
    compra_habitual = ("Patatas", "leche", "pan")

    especifica = []

    while True:
        elemento = input("Elemento a comprar: ")
        if not elemento:
            break
        especifica.append(elemento)

    especifica = list(set(especifica))

    compra_total = list(compra_habitual) + especifica

    # Eliminamos todos los elementos duplicados
    compra_total = borrar_duplicados(compra_total)

    # Sumamos todos los elementos de cada lista
    num_habitual = len(compra_habitual)
    num_especifico = len(set(especifica))
    num_total = len(compra_total)

    # Mostramos lista final
    print("Lista de compra: ")
    for elemento in compra_total:
        print(elemento)

    print(f"Elementos habituales: {num_habitual}")
    print(f"Elementos específicos: {num_especifico}")
    print(f"Elementos en lista: {num_total}")


if __name__ == '__main__':
    main()
